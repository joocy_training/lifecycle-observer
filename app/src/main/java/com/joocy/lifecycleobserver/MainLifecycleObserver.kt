package com.joocy.lifecycleobserver

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.util.Log

class MainLifecycleObserver: LifecycleObserver {

    companion object {
        val TAG = "MainLifecycleObserver"
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    fun somethingHappened(source: LifecycleOwner) {
        Log.d(TAG, "Something happened!!")
        Log.d(TAG, "The current lifecycle state is ${source.lifecycle.currentState}")
    }

}